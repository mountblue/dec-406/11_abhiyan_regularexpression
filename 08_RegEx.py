''' 
*Name:uppercase_lowercase.py
*Description:Find string with upper case followerd by lower
*Input : A string
*Output:If only contains ABcxc
*author:abhiyan timilsina
'''

import re
user_input = input('Enter a string : ')
matches = re.findall(r'[A-Z]+[a-z]+',user_input)
print(matches)