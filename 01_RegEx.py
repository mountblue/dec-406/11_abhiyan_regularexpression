''' 
*Name:checkForCharacters.py
*Description:Find wheather the string contains a ser of characters
*Input : A string
*Output:If only contains numbers and digits
*author:abhiyan timilsina
'''

import re

user_input =  input('Enter a string')

#Matching numbers in the string
unwanted_characters = re.findall(r'(^\d|[^a-z])',user_input)
if len(unwanted_characters)>0:
    print('Exists')
else :
    print('Onlt numbers and alphabets in the string')