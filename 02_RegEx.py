''' 
*Name:aFollowedbyb.py
*Description:Find wheather the string contains a's followed by b
*Input : A string
*Output:If only contains a..b
*author:abhiyan timilsina
'''

import re
user_input = input('Enter a string : ')
matches = re.findall(r'[Aa]+[Bb]*',user_input)
print(matches)